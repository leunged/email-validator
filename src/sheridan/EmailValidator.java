package sheridan;

public class EmailValidator {

	static boolean isValidFormat(String email) {
		String regex = "^(.+)@(.+)$";
		return email.matches(regex);
	}
	
	static boolean isValidAccountName(String email) {
		String regex = "^(([a-zA-Z].*?){3})@(.+)$";
		return email.matches(regex);
	}
	
	static boolean isValidDomainName(String email) {
		String regex = "^(.+)@(([a-zA-Z].*?){3})$";
		return email.matches(regex);
	}
	
	static boolean isValidAtSymbol(String email) {
		String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/="
				+ "?`{|}~^-]+)*@(?!-)(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
		return email.matches(regex);
	}
	
	static boolean isValidExtensionName(String email) {
		String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`"
				+ "{|}~^-]+)*@(?!-)(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
		return email.matches(regex);
	}
	
}
